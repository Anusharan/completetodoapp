<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Session;

class TodosController extends Controller
{
    //
    public function index()
    {
        //Extracting all datas using Todo model and return to todos view.
        $todos = Todo::all();
        return view('todos')->with('todos', $todos);

    }

    public function store(Request $request)
    {
        //To store data into database
        $toodo = new Todo;
        $toodo->todo = $request->todo;
        $toodo->save();

        //to dispalay successfull message
        Session::flash('success', 'Your todo was created successfully!!');

        // after storing redirect back to same page
        return redirect()->back();

    }

    public function delete($id)
    {
        $todo = Todo::find($id);
        $todo->delete();

        //to dispalay successfull message
        Session::flash('success', 'Your todo was deleted successfully!!');

        return redirect()->back();
    }

    public function update($id)
    {
        $todo = Todo::find($id);
        return view('update')->with('todo', $todo);
    }

    public function save(Request $request, $id)
    {
        $todo = Todo::find($id);
        $todo->todo = $request->todo;
        $todo->save();

        //to dispalay successfull message
        Session::flash('success', 'Your todo was updated successfully!!');

        return redirect()->route('todos');

    }

    public function completed($id)
    {
        $todo = Todo::find($id);
        $todo->completed = 1;
        $todo->save();

        //to dispalay successfull message
        Session::flash('success', 'Your todo was marked  successfully!!');

        return redirect()->back();
    }
}